<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
class RequestFormController extends Controller
{
    public function show() {
        return view('pages.form');
    }
    public function store(Request $request) {
        $product = new Product();
        $product -> name = $request -> name;
        $product -> description = $request -> description;
        $product -> updated_date = $request -> updated_date;
        return 1;
    }
}