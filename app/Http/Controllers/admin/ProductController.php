<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Pagination\Paginator;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(4);
        return view('pages.product')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.product');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $product = new Product();
        $product -> name = $request -> name;
        $product -> description = $request -> description;
        $product -> updated_date = $request -> updated_date;

        if($request -> hasfile('image')) {
            // $file = $request->file('image');
            $file = $request->image;
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/product/', $filename);
            $product->image = $filename;
        } else {
            return $request;
            $product->image = '';
        }
        $product -> save();
        return redirect() -> route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editproduct = Product::findorfail($id);
        return view('pages.editproduct',compact('editproduct'));
        // return view('pages.product')->with('product',$editproduct);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rec = Product::findorfail($id);
        $rec -> name = $request -> name;
        $rec -> description = $request -> description;
        $rec -> updated_date = $request -> updated_date;
        // $rec -> save();
        // return redirect() -> route('product.index');
        if($request -> hasfile('image')) {
            // $file = $request->file('image');
            $file = $request->image;
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/product/', $filename);
            $rec->image = $filename;
        } else {
            return $request;
            $rec->image = '';
        }
        $rec -> save();
        return redirect() -> route('product.index');
        // dd($request -> name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findorfail($id);
        $product -> delete();
        return redirect() -> route('product.index');
        // dd($product);
    }
}