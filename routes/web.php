<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/about','admin\AboutUsController@index');

// Form get
// Route::get('/form','admin\AboutUsController@show');

// Form post
// Route::post('/postformdata','admin\AboutUsController@postformdata');
// deleteitem
// Route::get('/deleteitem/{id}','admin\AboutUsController@delete');



//for update

//to get the data
// Route::get('/aboutedit/{id}','admin\AboutUsController@update');
// Route::post('/editformdata/{id}','admin\AboutUsController@updateformdata');



Auth::routes();

Route::resource('about', 'admin\AboutController');
Route::resource('product','admin\ProductController');
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' =>['auth'], 'prefix' => '' ], function() {
    Route::get('/','admin\indexController@index');
});