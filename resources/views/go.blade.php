@extends('welcome')

@section('content')

<div style="height: 80vh;" class="bg-white text-center d-flex flex-column justify-content-center align-items-center">
    <div class="card-title pt-4">
        To View Admin Dashboard
    </div>
    <div>
        <a style="font-size: 40px; text-decoration:none" class="text-uppercase" href=" {{ route('login') }} ">
            Login ?
        </a>
    </div>
</div>
@endsection