@extends('welcome')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Order Listing Table</h1>
    <p class="mb-4">These all are Order available.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Order</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <button type="button" class="btn btn-info mb-3" data-toggle="modal" data-target="#addOrder">Add
                    Order</button>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Updated Date</th>
                            <th>Function</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                        <tr>
                            <td> {{ $order-> id }} </td>
                            <td> {{ $order-> name }} </td>
                            <td> {{$order -> description}} </td>
                            <td> {{ $order -> updated_date }} </td>
                            <td>
                                <a class="btn btn-info text-capitalize btn-block" href="#">edit</a>
                                <a class="btn text-capitalize btn-danger btn-block" href="#">delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- /.container-fluid -->
@endsection

<!-- Modal -->
{{-- Add Product --}}
<div class="modal fade" id="addOrder" tabindex="-1" role="dialog" aria-labelledby="addOrderTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action=" {{ route('order.store') }} " id="form" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Enter order">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" name="description" class="form-control" id="description"
                            placeholder="Enter Description">
                    </div>
                    <div class="form-group">
                        <label for="updated_date">Updated Date</label>
                        <input type="date" name="updated_date" class="form-control" id="updated_date"
                            placeholder="Enter updated_date">
                    </div>
                    <button type="submit" class="btn btn-info mb-3">Add Product!!</button>
                    <button type="submit" class="btn btn-danger mb-3" data-dismiss="modal">Cancel Product!!</button>
                </form>
            </div>
        </div>
    </div>
</div>