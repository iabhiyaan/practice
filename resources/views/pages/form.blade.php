@extends('welcome')
@section('content')
<div class="container my-4">
    <div class="row">
        <div class="col-12">
            <h1>Add About</h1>
        </div>
        <!-- Insert -->
        <div class="col-7">
            <form action=" {{route('add')}} " role="form" enctype="multipart/form-data" method="POST">
                @csrf
                <div class="form-group">
                    <label for="heading">Heading:</label>
                    <input name="heading" type="text" class="form-control" placeholder="Enter Heading" id="heading">
                </div>
                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea name="description" type="text" rows="5" class="form-control"
                        placeholder="Enter Description" id="description"></textarea>
                </div>
                <div class="form-group">
                    <label for="status">status:</label>
                    <textarea name="status" type="text" rows="5" class="form-control"
                        placeholder="Enter status" id="status"></textarea>
                </div>
                <div class="form-group">
                    <label for="date">Updated date:</label>
                    <input name="date" type="date" class="form-control" placeholder="Enter Date" id="date">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection