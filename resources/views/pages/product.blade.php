@extends('welcome')
@section('content')
<!-- Begin Page Content -->

<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Product Listing Table</h1>
    <p class="mb-4">These all are products available.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Products</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-info mb-3" data-toggle="modal" data-target="#addProducts">
                    Add Products
                </button>
                <div class="modal fade" id="addProducts" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form id="addform" method="POST" action="{{ route('product.store')}}"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" class="form-control" id="name"
                                            placeholder="Enter product">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <input type="text" name="description" class="form-control" id="description"
                                            placeholder="Enter Description">
                                    </div>
                                    <div class="form-group">
                                        <label for="updated_date">Updated Date</label>
                                        <input type="date" name="updated_date" class="form-control" id="updated_date"
                                            placeholder="Enter updated_date">
                                    </div>
                                    <div class="form-group">
                                        <label for="image">Upload Image</label>
                                        <input type="file" id="image" name="image" class="form-control" />
                                    </div>

                                    <button type="submit" class="btn btn-info mb-3">Add Product!!</button>
                                    <button class="btn btn-danger mb-3" data-dismiss="modal">Cancel
                                        Product!!</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Updated Date</th>
                            <th>Image</th>
                            <th>Function</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- @if($products > 1)

                        @else
                        @php echo 'No data Found!!' @endphp
                        @endif --}}
                        @foreach ($products as $p)
                        <tr>
                            <td> {{ $p->id }} </td>
                            <td> {{ $p->name }} </td>
                            <td> {{ $p->description }} </td>
                            <td> {{ $p->updated_date }} </td>
                            <td>
                                <img style="width:120px" src="{{asset('uploads/product/'. $p->image)}}" />
                            </td>
                            <td>
                                {{-- <button type="button" class="btn btn-block btn-info mb-3" data-toggle="modal"
                                    data-target="#addProduct">Edit</button> --}}
                                <a href=" {{route('product.edit', $p->id)}}"
                                    class="btn btn-info mb-3 btn-block">Edit</a>
                                <form action="{{route('product.destroy', $p->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-block">Delete</button>
                                </form>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $products->links() }}
            </div>
        </div>
    </div>

</div>


<!-- /.container-fluid -->
@endsection



{{-- <div class="modal fade" id="editProduct" tabindex="-1" role="dialog" aria-labelledby="editProductTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('product.update', $editproduct->id)}}" id="form"
enctype="multipart/form-data">
@csrf
@method('PUT')
<div class="form-group">
    <label for="name">Name</label>
    <input value="{{$product->name}}" type="text" name="name" class="form-control" id="name"
        placeholder="Enter product">
</div>
<div class="form-group">
    <label for="description">Description</label>
    <input value="{{$editproduct->description}}" type="text" name="description" class="form-control" id="description"
        placeholder="Enter Description">
</div>
<div class="form-group">
    <label for="updated_date">Updated Date</label>
    <input type="date" name="updated_date" class="form-control" id="updated_date" placeholder="Enter updated_date">
</div>
<button type="submit" class="btn btn-info mb-3">Edit Product!!</button>
<button type="submit" class="btn btn-danger mb-3" data-dismiss="modal">Cancel Product!!</button>
</form>

</div>
</div>
</div>
</div> --}}