@extends('welcome')
@section('content')
<div class="container my-4">
    <div class="row">
        <div class="col-12">
            <h1>Edit Product</h1>
        </div>
        <!-- Update -->
        <div class="col-7">
            <form method="POST" action="{{route('product.update', $editproduct -> id)}}" id="form"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Name</label>
                    <input value="{{$editproduct->name}}" type="text" name="name" class="form-control" id="name"
                        placeholder="Enter product">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input value="{{$editproduct->description}}" type="text" name="description" class="form-control"
                        id="description" placeholder="Enter Description">
                </div>
                <div class="form-group">
                    <label for="updated_date">Updated Date</label>
                    <input type="date" value="{{ $editproduct -> updated_date }}" name="updated_date"
                        class="form-control" id="updated_date" placeholder="Enter updated_date">
                </div>
                <div class="form-group">
                    <label for="image">Edit Image</label>
                    <input type="file" name="image" value="{{ $editproduct->image }}" id="image">
                </div>
                <button type="submit" class="btn btn-info mb-3">Edit Product!!</button>
            </form>
        </div>
    </div>
</div>
@endsection