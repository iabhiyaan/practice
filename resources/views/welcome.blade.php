<!DOCTYPE html>
<html lang="en">

@include('includes.header')

<body id="page-top">
    <div id="wrapper">
        @include('includes.sidebar')
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                @include('includes.nav')
                <!-- End of Topbar -->
                
                @yield('content')
            </div>
            <!-- End of Main Content -->
        </div>
    </div>
    <!-- Page Wrapper -->
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up d-flex h-100 align-items-center justify-content-center"></i>
    </a>
    <div class="modal fade" id="addProduct" tabindex="-1" role="dialo">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="addform">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Enter product">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" name="description" class="form-control" id="description"
                                placeholder="Enter Description">
                        </div>
                        <div class="form-group">
                            <label for="updated_date">Updated Date</label>
                            <input type="date" name="updated_date" class="form-control" id="updated_date"
                                placeholder="Enter updated_date">
                        </div>
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input" id="inputGroupFile04"
                                    aria-describedby="inputGroupFileAddon04">
                                <label class="custom-file-label" for="inputGroupFile04">Choose file</label>
                            </div>
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button"
                                    id="inputGroupFileAddon04">Button</button>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info mb-3">Add Product!!</button>
                        <button type="submit" class="btn btn-danger mb-3" data-dismiss="modal">Cancel
                            Product!!</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    @include('includes.scripts')

</body>

</html>